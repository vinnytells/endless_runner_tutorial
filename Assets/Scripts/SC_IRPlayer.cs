﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

[System.Serializable]
public class Boundary
{
    public float xMax, xMin, yMax, yMin, zMax, zMin;
}

public class SC_IRPlayer : MonoBehaviour
{
    public float gravity = 20.0f;
    public float jumpHeight = 2.5f;
    public float speed = 5;
    public Boundary boundary;

    private Rigidbody rb;
    private bool grounded = false;
    private Vector3 defaultScale;
    private bool crouch = false;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        rb.freezeRotation = true;
        rb.useGravity = false;
        defaultScale = transform.localScale;
    }

    void Update()
    {
        // Move horizontally with the arrows or with the keys A and D
        // X axis - horizontally
        //*
        if (grounded)
        {
            float horizontal = Input.GetAxis("Horizontal") * speed;
            rb.velocity = new Vector3(horizontal, rb.velocity.y, rb.velocity.z);
            //rb.AddForce(horizontal, 0, 0, ForceMode.Impulse);
            //transform.Translate(horizontal, 0, 0);

            //
            float vertical = Input.GetAxis("Vertical");

            if(vertical>0)
                rb.velocity = new Vector3(rb.velocity.x, CalculateJumpVerticalSpeed(), rb.velocity.z);

            if (vertical<0)
            {
                crouch = (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow));
                if (crouch)
                {
                    transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(defaultScale.x, defaultScale.y * 0.4f, defaultScale.z), Time.deltaTime * 7);
                }
                else
                {
                    transform.localScale = Vector3.Lerp(transform.localScale, defaultScale, Time.deltaTime * 7);
                }
            }
        }
        //*/

        /*
        if ((Input.GetKey(KeyCode.A)) && grounded)
        {
            rb.velocity = new Vector3(speed , rb.velocity.y, rb.velocity.z);
        }

        if (Input.GetKey(KeyCode.D) && grounded)
        {
            rb.velocity = new Vector3(-speed, rb.velocity.y, rb.velocity.z);
        }
        //*/

        // Jump
        /*
        if ((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) && grounded)
        {
            rb.velocity = new Vector3(rb.velocity.x, CalculateJumpVerticalSpeed(), rb.velocity.z);
        }
        //*/

        //Crouch
        /*
        crouch = (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow));
        if (crouch)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(defaultScale.x, defaultScale.y * 0.4f, defaultScale.z), Time.deltaTime * 7);
        }
        else
        {
            transform.localScale = Vector3.Lerp(transform.localScale, defaultScale, Time.deltaTime * 7);
        }
        //*/
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // We apply gravity manually for more tuning control
        rb.AddForce(new Vector3(0, -gravity * rb.mass, 0));

        grounded = false;

        // Define player limit movimnents in scene
        rb.position = new Vector3(Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax), Mathf.Clamp(rb.position.y, boundary.yMin, boundary.yMax), Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax));
    }

    void OnCollisionStay()
    {
        grounded = true;
    }

    float CalculateJumpVerticalSpeed()
    {
        // From the jump height and gravity we deduce the upwards speed 
        // for the character to reach at the apex.
        return Mathf.Sqrt(2 * jumpHeight * gravity);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Finish")
        {
            //print("GameOver!");
            SC_GroundGenerator.instance.gameOver = true;
        }
    }
}